import 'package:dio/dio.dart';

class AuthInterceptor extends Interceptor {
  @override
  Future<void> onError(DioException err, ErrorInterceptorHandler handler) async {
    if (err.response?.statusCode == 401) {
      // Unauthenticated error, navigate to login page
      // Uncomment the following lines if you want to return to the original page after login
      // var currentRoute = MyApp().navigatorKey.currentState!.settings.name;
      // print('Current Route: $currentRoute');
      // await Get.toNamed('/login');

      // String currentRoute = Get.currentRoute;
      // await Get.offAllNamed('/login');
      // await Get.offAllNamed(currentRoute);

      // Cancel the request to prevent further processing
      return handler.reject(err);
    }

    // For other errors, let them propagate
    return super.onError(err, handler);
  }
}