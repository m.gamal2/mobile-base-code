import 'package:flutter/material.dart';

abstract class AppRouter {
  Route<dynamic>? onGenerateRoute(RouteSettings settings) {
    final Object? args = settings.arguments;
    if (settings.name == '/') {
      // return MaterialPageRoute(builder: (_) => const HomeScreen());
    }
    return null;
  }
}
