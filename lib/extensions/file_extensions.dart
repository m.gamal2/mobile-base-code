import 'dart:io';

extension FileSize on File {
  double fileSize() {
    final int sizeInBytes = lengthSync();
    final double sizeInMb = sizeInBytes / (1024 * 1024);
    return sizeInMb;
  }
}

extension CheckFileSize on File {
  bool isBigSize(int megabytes) {
    final File file = File(path);
    final int sizeInBytes = file.lengthSync();
    final double sizeInMb = sizeInBytes / (1024 * 1024);
    if (sizeInMb > 1) {
      return true;
    } else {
      return false;
    }
  }
}
