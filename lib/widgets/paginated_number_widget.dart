import 'package:flutter/material.dart';
import 'package:neoxero_basecode/main.dart';
import 'package:number_paginator/number_paginator.dart';

class PaginatedNumberWidget extends StatefulWidget {
  const PaginatedNumberWidget({Key? key}) : super(key: key);

  @override
  State<PaginatedNumberWidget> createState() => _PaginatedNumberWidgetState();
}

class _PaginatedNumberWidgetState extends State<PaginatedNumberWidget> {
  final List<List<TestDataModel>> paginatedData = [
    [
      TestDataModel(id: 1, name: 'Item 1'),
      TestDataModel(id: 2, name: 'Item 2'),
      TestDataModel(id: 3, name: 'Item 3'),
      TestDataModel(id: 4, name: 'Item 4'),
      TestDataModel(id: 5, name: 'Item 5'),
    ],
    [
      TestDataModel(id: 6, name: 'Item 6'),
      TestDataModel(id: 7, name: 'Item 7'),
      TestDataModel(id: 8, name: 'Item 8'),
      TestDataModel(id: 9, name: 'Item 9'),
      TestDataModel(id: 10, name: 'Item 10'),
    ],
    [
      TestDataModel(id: 11, name: 'Item 11'),
      TestDataModel(id: 12, name: 'Item 12'),
      TestDataModel(id: 13, name: 'Item 13'),
      TestDataModel(id: 14, name: 'Item 14'),
      TestDataModel(id: 15, name: 'Item 15'),
    ],
    [
      TestDataModel(id: 16, name: 'Item 16'),
      TestDataModel(id: 17, name: 'Item 17'),
      TestDataModel(id: 18, name: 'Item 18'),
      TestDataModel(id: 19, name: 'Item 19'),
      TestDataModel(id: 20, name: 'Item 20'),
    ],
    [
      TestDataModel(id: 21, name: 'Item 21'),
      TestDataModel(id: 22, name: 'Item 22'),
      TestDataModel(id: 23, name: 'Item 23'),
      TestDataModel(id: 24, name: 'Item 24'),
      TestDataModel(id: 25, name: 'Item 25'),
    ],
  ];

  final int _numPages = 5;
  int _currentPage = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: List.generate(
        _numPages,
        (index) => ListView.separated(
          itemCount: paginatedData[index].length,
          itemBuilder: (_, index) => Card(
            margin: const EdgeInsets.all(5),
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(paginatedData[_currentPage][index].name),
              ),
            ),
          ),
          separatorBuilder: (_, index) => const Divider(
            color: Colors.black26,
          ),
        ),
      )[_currentPage],
      // card for elevation
      bottomNavigationBar: Card(
        margin: const EdgeInsets.only(bottom: 20),
        elevation: 4,
        child: NumberPaginator(
          // by default, the paginator shows numbers as center content
          numberPages: _numPages,
          onPageChange: (int index) {
            setState(() {
              _currentPage = index;
            });
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          // Navigator.push(
          //     context,
          //     MaterialPageRoute(
          //         builder: (context) => const ScrollPositionScreen()));
        },
      ),
    );
  }
}

class TestDataModel {
  final int id;
  final String name;

  TestDataModel({required this.id, required this.name});
}
