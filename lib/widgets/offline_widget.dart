// import 'package:flutter/material.dart';
// import 'package:flutter_offline/flutter_offline.dart';
//
// class OfflineWidget extends StatelessWidget {
//   const OfflineWidget({Key? key, required this.widget}) : super(key: key);
//   final Widget widget;
//
//   @override
//   Widget build(BuildContext context) {
//     return OfflineBuilder(
//       connectivityBuilder: (
//         BuildContext context,
//         ConnectivityResult connectivity,
//         Widget child,
//       ) {
//         final bool connected = connectivity != ConnectivityResult.none;
//         return Stack(
//           fit: StackFit.expand,
//           children: [
//             ConnectionCheckableBar(connected: connected),
//             (connected)
//                 ? widget
//                 : Center(
//                     child: Image.asset(
//                       'assets/images/no-internet.gif',
//                       height: double.infinity,
//                       width: double.infinity,
//                     ),
//                   ),
//           ],
//         );
//       },
//       child: const SizedBox(),
//     );
//   }
// }
//
// enum ConnectionTypes { initial, connected, notConnected }
//
// class ConnectionCheckableBar extends StatefulWidget {
//   const ConnectionCheckableBar({Key? key, required this.connected})
//       : super(key: key);
//   final bool connected;
//
//   @override
//   State<ConnectionCheckableBar> createState() => _ConnectionCheckableBarState();
// }
//
// class _ConnectionCheckableBarState extends State<ConnectionCheckableBar> {
//   late ConnectionTypes connectionType;
//   late int lock;
//
//   @override
//   void initState() {
//     super.initState();
//     lock = 1;
//     _initializeConnectionType();
//   }
//
//   @override
//   void didUpdateWidget(covariant ConnectionCheckableBar oldWidget) {
//     _initializeConnectionType();
//     super.didUpdateWidget(oldWidget);
//   }
//
//   void _initializeConnectionType() {
//     connectionType = widget.connected
//         ? ConnectionTypes.connected
//         : ConnectionTypes.notConnected;
//     if (connectionType == ConnectionTypes.connected) {
//       Future.delayed(
//         const Duration(seconds: 3),
//         () => setState(() {
//           connectionType = ConnectionTypes.initial;
//         }),
//       );
//       lock++;
//     }
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Positioned(
//       height: 32.0,
//       left: 0.0,
//       right: 0.0,
//       child: (connectionType == ConnectionTypes.connected && lock != 1)
//           ? connectedBar()
//           : (connectionType == ConnectionTypes.notConnected)
//               ? notConnectedBar()
//               : emptyBar(),
//     );
//   }
//
//   Widget connectedBar() => AnimatedContainer(
//         duration: const Duration(milliseconds: 350),
//         color: const Color(0xFF00EE44),
//         child: const AnimatedSwitcher(
//           duration: Duration(milliseconds: 350),
//           child: Row(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: <Widget>[
//               Text('ONLINE'),
//             ],
//           ),
//         ),
//       );
//
//   Widget notConnectedBar() => AnimatedContainer(
//         duration: const Duration(milliseconds: 350),
//         color: const Color(0xFFEE4400),
//         child: const AnimatedSwitcher(
//           duration: Duration(milliseconds: 350),
//           child: Row(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: <Widget>[
//               Text('OFFLINE'),
//               SizedBox(width: 8.0),
//               SizedBox(
//                 width: 12.0,
//                 height: 12.0,
//                 child: CircularProgressIndicator(
//                   strokeWidth: 2.0,
//                   valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
//                 ),
//               ),
//             ],
//           ),
//         ),
//       );
//
//   Widget emptyBar() => AnimatedContainer(
//         duration: const Duration(milliseconds: 350),
//         color: Colors.transparent,
//         child: const AnimatedSwitcher(
//           duration: Duration(milliseconds: 350),
//           child: SizedBox(),
//         ),
//       );
// }
