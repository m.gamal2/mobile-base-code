import 'package:barcode_scan2/barcode_scan2.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:neoxero_basecode/helpers/code_scanner_helper.dart';

class ScanningWidget extends StatefulWidget {
  const ScanningWidget({Key? key}) : super(key: key);

  @override
  State<ScanningWidget> createState() => _ScanningWidgetState();
}

class _ScanningWidgetState extends State<ScanningWidget> {
  String? scanCode;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Barcode Scanner Example'),
        actions: [
          IconButton(
            icon: const Icon(Icons.document_scanner_outlined),
            tooltip: 'Scan',
            onPressed: () async {
              final result = await CodeScannerHelper.scan(autoEnableFlash: true);
              setState(() {
                scanCode = result.rawContent;
              });
            },
          ),
        ],
      ),
      body: Center(
        child: Text(
          scanCode ?? 'No Data',
          style: TextStyle(fontSize: 30),
        ),
      ),
    );
  }
}
