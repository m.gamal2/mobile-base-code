import 'package:map_launcher/map_launcher.dart';

abstract class MapLauncherHelper {
  static Future<List<AvailableMap>> getAvailableMaps() async {
    return await MapLauncher.installedMaps;
  }

  static Future<void> openMap({
    required AvailableMap map,
    required double latitude,
    required double longitude,
  }) async {
    try {
      if (await MapLauncher.isMapAvailable(map.mapType) == true) {
        await MapLauncher.showMarker(
          coords: Coords(latitude, longitude),
          mapType: map.mapType,
          title: map.mapName,
        );
      } else {
        throw 'Map not available';
      }
    } catch (e) {
      rethrow;
    }
  }
}

///Schemes for ios.
//<key>LSApplicationQueriesSchemes</key>
// <array>
//     <string>comgooglemaps</string>
//     <string>baidumap</string>
//     <string>iosamap</string>
//     <string>waze</string>
//     <string>yandexmaps</string>
//     <string>yandexnavi</string>
//     <string>citymapper</string>
//     <string>mapswithme</string>
//     <string>osmandmaps</string>
//     <string>dgis</string>
//     <string>qqmap</string>
//     <string>here-location</string>
//     <string>tomtomgo</string>
//     <string>copilot</string>
//     <string>com.sygic.aura</string>
// </array>
