import 'package:store_redirect/store_redirect.dart';

abstract class RedirectHelper {
  static Future<void> redirect(
      {required String androidAppId, required String iOSAppId}) async {
    try {
      await StoreRedirect.redirect(
        androidAppId: androidAppId,
        iOSAppId: iOSAppId,
      );
    } catch (e) {
      rethrow;
    }
  }
}
