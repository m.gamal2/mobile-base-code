import 'package:flutter/material.dart';
import 'navigation_helper.dart';

class DialogHelper {
  static final GlobalKey<NavigatorState> _navigationKey =
      NavigationHelper.navigationKey;

  void onShowDialog({required Widget dialog, required Color color}) =>
      showDialog(
        context: _navigationKey.currentContext!,
        builder: (_) => AlertDialog(
          backgroundColor: color,
          content: dialog,
        ),
      );

  void onShowLoading() => showGeneralDialog(
        context: _navigationKey.currentContext!,
        barrierDismissible: false,
        pageBuilder: (_, __, ___) =>
            const Center(child: CircularProgressIndicator()),
      );
}
