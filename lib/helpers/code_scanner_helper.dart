import 'package:barcode_scan2/barcode_scan2.dart';
import 'package:flutter/services.dart';

abstract class CodeScannerHelper {
  static bool initialized = false;

  static Future<void> initializeScanner() async {
    if (initialized != true) {
      await getNumberOfCameras();
      initialized = true;
    }
  }

  static final List<BarcodeFormat> possibleFormats = BarcodeFormat.values
      .toList()
    ..removeWhere((e) => e == BarcodeFormat.unknown);

  static final List<BarcodeFormat> selectedFormats = [...possibleFormats];

  static Future<int> getNumberOfCameras() async =>
      await BarcodeScanner.numberOfCameras;

  static Future<ScanResult> scan({
    aspectTolerance = 0.00,
    selectedCamera = -1,
    useAutoFocus = true,
    autoEnableFlash = false,
  }) async {
    try {
      final result = await BarcodeScanner.scan(
        options: ScanOptions(
          restrictFormat: selectedFormats,
          useCamera: selectedCamera,
          autoEnableFlash: autoEnableFlash,
          android: AndroidOptions(
            aspectTolerance: aspectTolerance,
            useAutoFocus: useAutoFocus,
          ),
        ),
      );
      return result;
    } on PlatformException {
      throw Exception('The user did not grant the camera permission!');
    } catch (e) {
      rethrow;
    }
  }
}
