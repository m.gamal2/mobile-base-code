// import 'dart:io';
// import 'dart:typed_data';
// import 'dart:convert';
// import 'package:pointycastle/export.dart';
//
// class FileCryptoHelper {
//   void encryptFile(Uint8List key, File inputFile, File outputFile) {
//     final cipher = BlockCipher('AES')..init(true, KeyParameter(key));
//     final inputBytes = inputFile.readAsBytesSync();
//     final encryptedBytes = cipher.process(Uint8List.fromList(inputBytes));
//     outputFile.writeAsBytesSync(encryptedBytes);
//   }
//   void decryptFile(Uint8List key, File encryptedFile, File decryptedFile) {
//     final cipher = BlockCipher('AES')..init(false, KeyParameter(key));
//     final encryptedBytes = encryptedFile.readAsBytesSync();
//     final decryptedBytes = cipher.process(Uint8List.fromList(encryptedBytes));
//     decryptedFile.writeAsBytesSync(decryptedBytes);
//   }
// }
//
// void test(){
//   File file1 = File('test.txt');
//   File file2 = File('test2.txt');
//   FileCryptoHelper().encryptFile(Uint8List.fromList('1234567890123456'.codeUnits), file1, file2);
// }

import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';
import 'package:crypto/crypto.dart';
import 'package:pointycastle/export.dart';

abstract class TextCryptoHelper {
  static String encryptText(String plainText, String key) {
    final keyBytes = utf8.encode(key);
    final plaintextBytes = utf8.encode(plainText);

    final keyDigest = sha256.convert(keyBytes);
    final keyDerived = Uint8List.fromList(keyDigest.bytes);

    final params = KeyParameter(keyDerived);
    final cipher = AESFastEngine()..init(true, params);

    final encryptedBytes = cipher.process(Uint8List.fromList(plaintextBytes));

    return base64Encode(encryptedBytes);
  }

  static String decryptText(String encryptedText, String key) {
    final keyBytes = utf8.encode(key);
    final encryptedBytes = base64Decode(encryptedText);

    final keyDigest = sha256.convert(keyBytes);
    final keyDerived = Uint8List.fromList(keyDigest.bytes);

    final params = KeyParameter(keyDerived);
    final cipher = AESFastEngine()..init(false, params);

    final decryptedBytes = cipher.process(Uint8List.fromList(encryptedBytes));

    return utf8.decode(decryptedBytes);
  }

  static Uint8List generateRandomKey() {
    final random = Random.secure();
    final key = List<int>.generate(32, (_) => random.nextInt(256));
    return Uint8List.fromList(key);
  }
}
