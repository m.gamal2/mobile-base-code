import 'package:file_picker/file_picker.dart';

abstract class FilePickerHelper {
  static final FilePicker picker = FilePicker.platform;

  static Future<PlatformFile?> pickSingleFile() async {
    PlatformFile? file;
    try {
      FilePickerResult? result = await picker.pickFiles(
        type: FileType.custom,
        allowedExtensions: ['pdf', 'jpg', 'jpeg', 'png'],
      );
      file = result?.files.single;
    } catch (e) {
      rethrow;
    }
    return file;
  }

  static Future<List<PlatformFile>> pickMultipleFiles() async {
    final List<PlatformFile> files = [];
    try {
      FilePickerResult? result = await picker.pickFiles(
        allowMultiple: true,
        type: FileType.custom,
        allowedExtensions: ['pdf', 'jpg', 'jpeg', 'png'],
      );
      if (result != null) {
        files.addAll(result.files);
      } else {
        files.clear();
      }
    } catch (e) {
      rethrow;
    }
    return files;
  }
}
