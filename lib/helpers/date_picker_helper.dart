import 'package:flutter/material.dart';
import 'navigation_helper.dart';

abstract class DatePickerHelper {
  static final GlobalKey<NavigatorState> _navigationKey =
      NavigationHelper.navigationKey;

   static Future<DateTime?> onShowDatePicker() async => await showDatePicker(
        context: _navigationKey.currentContext!,
        initialDate: DateTime.now(),
        firstDate: DateTime(1950),
        lastDate: DateTime.now(),
      );

   static Future<TimeOfDay?> onShowTimePicker() async => await showTimePicker(
        context: _navigationKey.currentContext!,
        initialTime: TimeOfDay.now(),
      );
}
