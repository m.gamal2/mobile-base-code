import 'package:record/record.dart';

abstract class RecorderHelper {
  static final Record _record = Record();

  static String? path;
  static bool? isPaused;

  static Future<bool> checkRecorderPermissions() async {
    try {
      return await _record.hasPermission();
    } catch (e) {
      rethrow;
    }
  }

  static Future<bool> get isRecording async => await _record.isRecording();

  static void startRecord() async {
    try {
      await _record.start(
        encoder: AudioEncoder.aacLc,
        bitRate: 128000,
        samplingRate: 44100,
      );
    } catch (e) {
      rethrow;
    }
  }

  static void stopRecord() async {
    try {
      final recording = await isRecording;
      if (recording) {
        path = await _record.stop();
      }
    } catch (e) {
      rethrow;
    }
  }

  static void pauseRecord() async {
    try {
      final recording = await isRecording;
      if (recording) {
        await _record.pause();
        isPaused = true;
      }
    } catch (e) {
      rethrow;
    }
  }

  static void resumeRecord() async {
    try {
      if (isPaused == true) {
        await _record.resume();
        isPaused = false;
      }
    } catch (e) {
      rethrow;
    }
  }

  ///Android permissions.
// <uses-permission android:name="android.permission.RECORD_AUDIO" />
// <!-- Optional, you'll have to check this permission by yourself. -->
// <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />

  ///IOS permissions.
// <key>NSMicrophoneUsageDescription</key>
// <string>We need to access to the microphone to record audio file</string>
}
