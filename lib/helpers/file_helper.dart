import 'dart:developer';
import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

abstract class FileHelper {
  static Future<void> downloadFile({required String url}) async {
    final status = await _requestStoragePermission();
    if (status.isGranted) {
      try {
        final bytes = await _getFile(url: url);
        await _storeFile(url: url, bytes: bytes);
      } catch (e) {
        rethrow;
      }
    }
  }

  static Future<PermissionStatus> _requestStoragePermission() async {
    if (Platform.isAndroid) {
      final plugin = DeviceInfoPlugin();
      final android = await plugin.androidInfo;
      if (android.version.sdkInt < 33) {
        return await Permission.storage.request();
      } else {
        return PermissionStatus.granted;
      }
    }

    return await Permission.storage.status;
  }

  static Future<List<int>> _getFile({required String url}) async {
    final response = await http.get(Uri.parse(url));
    return response.bodyBytes;
  }

  static Future<void> _storeFile(
      {required String url, required List<int> bytes}) async {
    try {
      final fileName = basename(url);
      final Directory directory = Platform.isAndroid

          ///if you want to save file on Download folder
          ? Directory('/storage/emulated/0/Download')

          ///if you want to save file on private folder
          // await getExternalStorageDirectory()
          : await getApplicationDocumentsDirectory();
      if (!await directory.exists()) {
        await directory.create(recursive: true);
      }

      final File file = File('${directory.path}/$fileName');

      if (await file.exists()) {
        await file.delete();
      }
      final storedFile = await file.writeAsBytes(bytes, flush: true);
      log(storedFile.path);
    } catch (e) {
      rethrow;
    }
  }
}

///Android setup
// <uses-permission android:name="android.permission.INTERNET" />
// <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
// <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/>

///iOS setup
// <keystorage>
//   <key>NSFileProtectionKey</key>
//   <string>NSFileProtectionNone</string>
// </keystorage>
