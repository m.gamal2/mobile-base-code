import 'dart:io';

import 'package:dio/dio.dart';
import 'package:neoxero_basecode/interceptors/auth_interceptor.dart';

abstract class DioHelper {
  static final Dio _dio = Dio();

  static Future<Response> getData({
    required String url,
    Options? options,
  }) async {
    _dio.interceptors.add(AuthInterceptor());
    return await _dio.get(url, options: options);
  }

  static Future<Response> postData({
    required String url,
    dynamic data,
    Options? options,
  }) async {
    _dio.interceptors.add(AuthInterceptor());
    return await _dio.post(url, data: data, options: options);
  }

  static Future<Response> putData({
    required String url,
    required dynamic data,
    Options? options,
  }) async {
    _dio.interceptors.add(AuthInterceptor());
    return await _dio.put(url, data: data, options: options);
  }

  static Future<Response> deleteData({
    required String url,
    dynamic data,
    Options? options,
  }) async {
    _dio.interceptors.add(AuthInterceptor());
    return await _dio.delete(url, data: data, options: options);
  }

  static Future<Response> downloadData({
    required String url,
    required String filePath,
  }) async {
    _dio.interceptors.add(AuthInterceptor());
    return await _dio.download(url, filePath);
  }

  static Future<Response> uploadImage({
    required File image,
    required String url,
    required Map<String, dynamic> data,
    required String imageJsonKey,
    Options? options,
  }) async {
    final String fileName = image.path.split('/').last;
    final multiPartFile =
        await MultipartFile.fromFile(image.path, filename: fileName);
    final FormData formData = FormData.fromMap(data);
    formData.files.add(MapEntry(imageJsonKey, multiPartFile));
    return await postData(url: url, data: formData, options: options);
  }

  static Future<MultipartFile> multiPartFile({required File file}) async {
    final String fileName = file.path.split('/').last;
    return await MultipartFile.fromFile(file.path, filename: fileName);
  }

  static Future<Response> uploadMultipleFile({
    required String url,
    required List<File> images,
    required Map<String, dynamic> data,
    required String imagesJsonKey,
    Options? options,
  }) async {
    final FormData formData = FormData.fromMap(data);
    for (File item in images) {
      final String fileName = item.path.split('/').last;
      final multipartFile =
          await MultipartFile.fromFile(item.path, filename: fileName);
      formData.files.addAll([MapEntry(imagesJsonKey, multipartFile)]);
    }
    return await postData(url: url, data: formData, options: options);
  }

  static String handlingStatusCodeFailures({
    required Response? response,
    String? error,
  }) {
    switch (response?.statusCode) {
      case 400:
        return 'BadRequestFailure';
      case 401:
        return error ?? 'UnauthorizedFailure';
      case 403:
        return 'ForbiddenFailure';
      case 404:
        return 'NotFoundFailure';
      case 409:
        return 'ConflictFailure';
      case 500:
        return 'InternalServerErrorFailure';
      case 503:
        return 'ServiceUnavailableFailure';
      default:
        return error ?? 'UnknownFailure';
    }
  }

  static String handlingDioFailures({
    required DioExceptionType dioExceptionType,
    Response? response,
    String? error,
  }) {
    switch (dioExceptionType) {
      case DioExceptionType.badCertificate:
        return 'BadCertificateFailure';
      case DioExceptionType.connectionTimeout:
        return 'ConnectTimeoutFailure';
      case DioExceptionType.connectionError:
        return 'ConnectionErrorFailure';
      case DioExceptionType.sendTimeout:
        return 'SendTimeoutFailure';
      case DioExceptionType.receiveTimeout:
        return 'ReceiveTimeoutFailure';
      case DioExceptionType.badResponse:
        return handlingStatusCodeFailures(response: response, error: error);
      case DioExceptionType.cancel:
        return 'CancelRequestFailure';
      case DioExceptionType.unknown:
        return 'UnKnownFailure';
      default:
        return 'UnKnownFailure';
    }
  }
}
