import 'package:flutter/material.dart';

abstract class NavigationHelper {
  static final GlobalKey<NavigatorState> navigationKey =
      GlobalKey<NavigatorState>();

  static Future<dynamic> onNavigate({required Widget page}) =>
      navigationKey.currentState!.push(MaterialPageRoute(builder: (_) => page));

  static Future<dynamic> onReplacementNavigate({required Widget page}) =>
      navigationKey.currentState!
          .pushReplacement(MaterialPageRoute(builder: (_) => page));

  static Future<dynamic> onNavigateAndRemoveUntil({required Widget page}) =>
      navigationKey.currentState!.pushAndRemoveUntil(
          MaterialPageRoute(builder: (_) => page), (route) => false);

  static void onPop() => navigationKey.currentState!.pop();
}
