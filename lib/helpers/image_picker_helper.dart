import 'dart:io';
import 'package:image_picker/image_picker.dart';

abstract class ImagePickerHelper {
  static final ImagePicker _picker = ImagePicker();

  static Future<File?> pickImage(
      {ImageSource source = ImageSource.gallery,
      int? quality,
      double? maxHeight,
      double? maxWidth}) async {
    try {
      final XFile? xFile = await _picker.pickImage(
          source: source,
          imageQuality: quality,
          maxHeight: maxHeight,
          maxWidth: maxWidth);
      return xFile != null ? File(xFile.path) : null;
    } catch (e) {
      rethrow;
    }
  }

  static Future<List<File?>> pickMultipleImages(
      {int? quality, double? maxHeight, double? maxWidth}) async {
    try {
      final List<XFile?> xFiles = await _picker.pickMultiImage(
          imageQuality: quality, maxHeight: maxHeight, maxWidth: maxWidth);
      final List<File?> files =
          xFiles.map((e) => e != null ? File(e.path) : null).toList();
      return files;
    } catch (e) {
      rethrow;
    }
  }
}
