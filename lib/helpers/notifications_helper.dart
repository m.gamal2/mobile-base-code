import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart' as timezone;
import 'package:flutter/foundation.dart';

abstract class NotificationsHelper {
  static final FirebaseMessaging _firebaseMessaging =
      FirebaseMessaging.instance;
  static final FlutterLocalNotificationsPlugin _localNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  static bool initialized = false;

  static Future<void> initializeNotifications() async {
    if (initialized != true) {
      await _initializeFirebaseMessaging();
      await _initializeLocalNotifications();
      initialized = true;
    }
  }

  static Future<String?> getDeviceToken() async {
    try {
      final String? token = await _firebaseMessaging.getToken();
      return token;
    } catch (exception) {
      return null;
    }
  }

  static void onBackgroundMessage() =>
      FirebaseMessaging.onBackgroundMessage(_onBackgroundMessageHandler);

  static Future<void> _initializeFirebaseMessaging() async {
    final settings = await _setNotificationSettings();
    if (settings != null) {
      if (settings.authorizationStatus == AuthorizationStatus.authorized) {
        await _onTerminatedMessagesHandler();
        await _onMessageHandler();
        await _onMessageOpenedHandler();
      }
    }
  }

  static Future<NotificationSettings?> _setNotificationSettings() async {
    try {
      final NotificationSettings settings =
          await _firebaseMessaging.requestPermission(
        alert: true,
        badge: true,
        sound: true,
        announcement: false,
        carPlay: false,
        criticalAlert: false,
        provisional: false,
      );
      return settings;
    } catch (exception) {
      return null;
    }
  }

  static Future<void> _onBackgroundMessageHandler(RemoteMessage message) async {
    _showNotification(
        id: 0,
        title: message.notification?.title,
        body: message.notification?.body);
    if (kDebugMode) {
      print('onBackgroundMessage');
      print('data => ${message.data}');
      print('notification => ${message.notification?.title}');
    }
  }

  static Future<void> _onTerminatedMessagesHandler() async {
    final RemoteMessage? message = await _firebaseMessaging.getInitialMessage();
    _showNotification(
        id: 0,
        title: message?.notification?.title,
        body: message?.notification?.body);
    if (kDebugMode) {
      print('onTerminatedMessagesHandler');
      print('data => ${message?.data}');
      print('notification => ${message?.notification?.title}');
    }
  }

  static Future<void> _onMessageHandler() async {
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      _showNotification(
          id: 0,
          title: message.notification?.title,
          body: message.notification?.body);
      if (kDebugMode) {
        print('onMessage');
        print('data => ${message.data}');
        print('notification => ${message.notification?.title}');
      }
    });
  }

  static Future<void> _onMessageOpenedHandler() async {
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      _showNotification(
          id: 0,
          title: message.notification?.title,
          body: message.notification?.body);
      if (kDebugMode) {
        print('onMessageOpenedApp');
        print('data => ${message.data}');
        print('notification => ${message.notification?.title}');
      }
    });
  }

  static Future<void> _initializeLocalNotifications() async {
    const AndroidInitializationSettings androidSettings =
        AndroidInitializationSettings('@mipmap/ic_launcher');
    const DarwinInitializationSettings iosSettings =
        DarwinInitializationSettings(
      requestAlertPermission: true,
      requestBadgePermission: true,
      requestSoundPermission: true,
      // onDidReceiveLocalNotification: onDidReceiveLocalNotification,
    );
    const InitializationSettings initializationSettings =
        InitializationSettings(
      android: androidSettings,
      iOS: iosSettings,
    );
    _localNotificationsPlugin.initialize(initializationSettings);

    /// Initialize timezone
    // tz.initializeTimeZones();
  }

  static void _showNotification({
    int id = 0,
    required String? title,
    required String? body,
    String? payload,
  }) async {
    const AndroidNotificationDetails androidNotificationDetails =
        AndroidNotificationDetails(
      'channel id',
      'channel name',
      channelDescription: 'channel description',
      playSound: true,
      importance: Importance.max,
      priority: Priority.high,
    );
    const DarwinNotificationDetails iosNotificationDetails = DarwinNotificationDetails(
        // presentAlert: bool ?,
        /// => Present an alert when the notification is displayed and the application is in the foreground (only from iOS 10 onwards)
        // presentBadge: bool ?,
        /// => Present the badge number when the notification is displayed and the application is in the foreground (only from iOS 10 onwards)
        // presentSound: bool ?,
        /// => Play a sound when the notification is displayed and the application is in the foreground (only from iOS 10 onwards)
        // sound: String ?,
        /// => Specifics the file path to play (only from iOS 10 onwards)
        // badgeNumber: int ?,
        /// => The application's icon badge number
        // attachments: List<IOSNotificationAttachment> ?,
        /// => (only from iOS 10 onwards)
        // subtitle: String?,
        /// => Secondary description  (only from iOS 10 onwards)
        // threadIdentifier: String ?
        /// => (only from iOS 10 onwards),
        );

    const NotificationDetails notificationDetails = NotificationDetails(
        android: androidNotificationDetails, iOS: iosNotificationDetails);

    /// Payload => Show a notification with an optional payload that will be passed back to the app when a notification is tapped.
    _localNotificationsPlugin.show(id, title, body, notificationDetails,
        payload: payload);
  }

  /// This allows us to see our notification at a scheduled time, be it 5secs, 10mins, 1hr etc later.
  static void showTimedNotification(
      {int id = 0,
      required String title,
      required String body,
      String? payload,
      Duration? duration}) async {
    await _localNotificationsPlugin.zonedSchedule(
      id,
      title,
      body,
      timezone.TZDateTime.now(timezone.local)
          .add(duration ?? const Duration(seconds: 5)),
      const NotificationDetails(
        android: AndroidNotificationDetails(
          'channel_id',
          'channel Name',
        ),
      ),
      // androidAllowWhileIdle: true,
      androidScheduleMode: AndroidScheduleMode.exactAllowWhileIdle,
      // androidAllowWhileIdle: true deprecated
      uiLocalNotificationDateInterpretation:
          UILocalNotificationDateInterpretation.absoluteTime,
      matchDateTimeComponents: DateTimeComponents.time,
      // To implement showing notifications every day at custom time not show after this time only one.
      payload: payload,
    );
  }

  /// Set right date and time for notifications
  timezone.TZDateTime _convertTime(int hour, int minutes) {
    final timezone.TZDateTime now = timezone.TZDateTime.now(timezone.local);
    timezone.TZDateTime scheduleDate = timezone.TZDateTime(
      timezone.local,
      now.year,
      now.month,
      now.day,
      hour,
      minutes,
    );
    if (scheduleDate.isBefore(now)) {
      scheduleDate = scheduleDate.add(const Duration(days: 1));
    }
    return scheduleDate;
  }

  /// display a dialog with the notification details, tap ok to go to another page.
// void onDidReceiveLocalNotification(
//     int id, String title, String body, String payload) async {
//   showDialog(
//     context: context,
//     builder: (BuildContext context) => CupertinoAlertDialog(
//       title: Text(title),
//       content: Text(body),
//       actions: [
//         CupertinoDialogAction(
//           isDefaultAction: true,
//           child: Text('Ok'),
//           onPressed: () async {
//             Navigator.of(context, rootNavigator: true).pop();
//             await Navigator.push(
//               context,
//               MaterialPageRoute(
//                 builder: (context) => SecondScreen(payload),
//               ),
//             );
//           },
//         )
//       ],
//     ),
//   );
// }
}

///calling on main
// WidgetsFlutterBinding.ensureInitialized();
// await Firebase.initializeApp();
// NotificationsHelper.onBackgroundMessage();

///Android setup firebase messaging.
// <intent-filter>
// <action android:name="FLUTTER_NOTIFICATION_CLICK" />
// <category android:name="android.intent.category.DEFAULT" />
// </intent-filter>

// <meta-data
// android:name="com.google.firebase.messaging.default_notification_channel_id"
// android:value="high_importance_channel" />

///IOP setup firebase messaging.
//https://learn.buildfire.com/en/articles/5760994-how-to-set-up-your-apple-push-notification-key-for-your-ios-firebase-certificate

///Android setup flutter local notifications.
// <uses-permission android:name="android.permission.INTERNET"/>
// <uses-permission android:name="android.permission.POST_NOTIFICATIONS"/>
//
// compileSdkVersion 33
//
// To schedule notifications the following changes are needed
// <receiver android:exported="false" android:name="com.dexterous.flutterlocalnotifications.ScheduledNotificationReceiver" />
// <receiver android:exported="false" android:name="com.dexterous.flutterlocalnotifications.ScheduledNotificationBootReceiver">
// <intent-filter>
// <action android:name="android.intent.action.BOOT_COMPLETED"/>
// <action android:name="android.intent.action.MY_PACKAGE_REPLACED"/>
// <action android:name="android.intent.action.QUICKBOOT_POWERON" />
// <action android:name="com.htc.intent.action.QUICKBOOT_POWERON"/>
// </intent-filter>
// </receiver>

///IOP setup flutter local notifications.
// import UIKit
// import Flutter
// import flutter_local_notifications
//
// @UIApplicationMain
// @objc class AppDelegate: FlutterAppDelegate {
//
// override func application(
// _ application: UIApplication,
// didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
// ) -> Bool {
// // This is required to make any communication available in the action isolate.
// FlutterLocalNotificationsPlugin.setPluginRegistrantCallback { (registry) in
// GeneratedPluginRegistrant.register(with: registry)
// }
//
// if #available(iOS 10.0, *) {
// UNUserNotificationCenter.current().delegate = self as UNUserNotificationCenterDelegate
// }
//
// GeneratedPluginRegistrant.register(with: self)
// return super.application(application, didFinishLaunchingWithOptions: launchOptions)
// }
// }

// <key>UIBackgroundModes</key>
// <array>
// <string>fetch</string>
// <string>remote-notification</string>
// </array>
