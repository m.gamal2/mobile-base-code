// import 'package:flutter/material.dart';
// import 'package:neoxero_basecode/widgets/paginated_number_widget.dart';
// import 'package:shared_preferences/shared_preferences.dart';
//
// void main() => runApp(const MyApp());
//
// class MyApp extends StatelessWidget {
//   const MyApp({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       theme: ThemeData.from(
//         colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.blue),
//       ),
//       home: DefaultTabController(
//         length: 4,
//         child: Scaffold(
//           appBar: AppBar(
//             title: const Text("Number Paginator"),
//             bottom: const TabBar(
//               tabs: [
//                 Tab(child: Text("Hidden")),
//                 Tab(child: Text("Numbers")),
//                 Tab(child: Text("Dropdown")),
//                 Tab(child: Text("Builder")),
//               ],
//             ),
//           ),
//           body: const PaginatedNumberWidget(),
//         ),
//       ),
//     );
//   }
// }
//
// class ScrollPositionScreen extends StatefulWidget {
//   const ScrollPositionScreen({super.key});
//
//   @override
//   _ScrollPositionScreenState createState() => _ScrollPositionScreenState();
// }
//
// class _ScrollPositionScreenState extends State<ScrollPositionScreen> {
//   final ScrollController _scrollController = ScrollController();
//   double savedScrollPosition = 0.0;
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         automaticallyImplyLeading: true,
//         title: const Text('Auto Scroll Example'),
//       ),
//       body: AutoSavedScrollWidget(),
//     );
//   }
// }
//
// class AutoSavedScrollWidget extends StatefulWidget {
//   const AutoSavedScrollWidget({Key? key}) : super(key: key);
//   @override
//   State<AutoSavedScrollWidget> createState() => _AutoSavedScrollWidgetState();
// }
//
// class _AutoSavedScrollWidgetState extends State<AutoSavedScrollWidget> {
//   final ScrollController _scrollController = ScrollController();
//   double savedScrollPosition = 0.0;
//
//   @override
//   void initState() {
//     super.initState();
//     _loadScrollPosition();
//   }
//
//   @override
//   void didChangeDependencies() {
//     super.didChangeDependencies();
//     _scrollController.addListener(_saveScrollPosition);
//   }
//
//   @override
//   void dispose() {
//     _scrollController.dispose();
//     super.dispose();
//   }
//
//   void _loadScrollPosition() async {
//     final prefs = await SharedPreferences.getInstance();
//     setState(() {
//       savedScrollPosition = prefs.getDouble('scroll_position') ?? 0.0;
//     });
//     // Set savedScrollPosition to 0.0 initially
//     if (savedScrollPosition == 0.0) {
//       _scrollController.jumpTo(0.0);
//     } else {
//       // _scrollController.jumpTo(savedScrollPosition);
//       _scrollController.animateTo(
//         savedScrollPosition,
//         duration: const Duration(seconds: 2),
//         curve: Curves.fastOutSlowIn,
//       );
//     }
//   }
//
//   void _saveScrollPosition() async {
//     final prefs = await SharedPreferences.getInstance();
//     prefs.setDouble('scroll_position', _scrollController.position.pixels);
//   }
//
//   void _scrollDown() {
//     _scrollController.animateTo(
//       _scrollController.position.maxScrollExtent,
//       duration: const Duration(seconds: 2),
//       curve: Curves.fastOutSlowIn,
//     );
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return ListView.separated(
//       physics: const BouncingScrollPhysics(),
//       controller: _scrollController,
//       padding: const EdgeInsets.symmetric(horizontal: 15),
//       itemCount: 100,
//       itemBuilder: (context, index) {
//         return Container(
//           height: 150,
//           width: double.infinity,
//           decoration: BoxDecoration(
//             borderRadius: BorderRadius.circular(15),
//             color: Colors.blue.withOpacity(.5),
//           ),
//           child: Center(child: Text('Item $index')),
//         );
//       },
//       separatorBuilder: (_, index) => const SizedBox(
//         height: 15,
//       ),
//     );
//   }
// }

import 'dart:async';
import 'dart:io' show Platform;

import 'package:barcode_scan2/barcode_scan2.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:neoxero_basecode/widgets/scanning_widget.dart';

void main() => runApp(const App());

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);
  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {
  ScanResult? scanResult;

  final _flashOnController = TextEditingController(text: 'Flash on');
  final _flashOffController = TextEditingController(text: 'Flash off');
  final _cancelController = TextEditingController(text: 'Cancel');

  var _aspectTolerance = 0.00;
  var _numberOfCameras = 0;
  var _selectedCamera = -1;
  var _useAutoFocus = true;
  var _autoEnableFlash = false;

  static final _possibleFormats = BarcodeFormat.values.toList()
    ..removeWhere((e) => e == BarcodeFormat.unknown);

    List<BarcodeFormat> selectedFormats = [..._possibleFormats];

  @override
  void initState() {
    super.initState();

    Future.delayed(Duration.zero, () async {
      _numberOfCameras = await BarcodeScanner.numberOfCameras;
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: ScanTest(),
    );
  }

  Future<void> _scan() async {
    try {
      final result = await BarcodeScanner.scan(
        options: ScanOptions(
          strings: {
            'cancel': _cancelController.text,
            'flash_on': _flashOnController.text,
            'flash_off': _flashOffController.text,
          },
          restrictFormat: selectedFormats,
          useCamera: _selectedCamera,
          autoEnableFlash: _autoEnableFlash,
          android: AndroidOptions(
            aspectTolerance: _aspectTolerance,
            useAutoFocus: _useAutoFocus,
          ),
        ),
      );
      setState(() => scanResult = result);
    } on PlatformException catch (e) {
      setState(() {
        scanResult = ScanResult(
          rawContent: e.code == BarcodeScanner.cameraAccessDenied
              ? 'The user did not grant the camera permission!'
              : 'Unknown error: $e',
        );
      });
    }
  }
}


class ScanTest extends StatelessWidget {
  const ScanTest({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Barcode Scanner Example'),
        actions: [
          IconButton(
            icon: const Icon(Icons.camera),
            tooltip: 'Scan',
            onPressed: () => Navigator.push(context,  MaterialPageRoute(builder: (_) => const ScanningWidget())),
          ),
        ],
      ),
    );
  }
}
